'use strict';

// Options
// -------

// Default Options.
var BUILD_FOLDER = '../'; // Relativ to gulp file with ending Slash
var SCRIPT_CSS_FOLDER = './'; // Relativ to gulp file with ending Slash
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// GULP
// ----

// Include GULP modules.
var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var removeBom = require('fd-gulp-removebom');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var postcss = require('gulp-postcss');
var reporter = require('postcss-reporter');
var syntax_scss = require('postcss-scss');
var stylelint = require('stylelint');

// SASS
gulp.task('sass', function() {
  gulp.src(SCRIPT_CSS_FOLDER + '_scss/app.scss').pipe(sass({
    outputStyle : 'compressed'
  }).on('error', sass.logError)).pipe(autoprefixer({
    browsers : COMPATIBILITY
  })).pipe(removeBom()).pipe(rename("theme.min.css")).pipe(gulp.dest(BUILD_FOLDER + 'css'));
  gulp.start('sass-lint');
});

gulp.task("sass-lint", function() {
  // Stylelint config rules
  var stylelintConfig = {
    "rules": {
      "block-no-empty": true,
      "color-no-invalid-hex": true,
      "declaration-colon-space-after": "always",
      "declaration-colon-space-before": "never",
      "function-comma-space-after": "always",
      "function-url-quotes": "single",
      "media-feature-colon-space-after": "always",
      "media-feature-colon-space-before": "never",
      "media-feature-name-no-vendor-prefix": true,
      "max-empty-lines": 5,
      "number-leading-zero": "always",
      "number-no-trailing-zeros": true,
      "selector-list-comma-space-before": "never",
      "selector-list-comma-newline-after": "always",
      "selector-no-id": true,
      "string-quotes": "single"
    }
  }

  var processors = [
    stylelint(stylelintConfig),
    reporter({
      clearMessages: true,
      throwError: true
    })
  ];
  
  var sass_folder = [
    SCRIPT_CSS_FOLDER + '_scss/**/*.scss',
    '!_scss/app.scss',
    '!**/_settings_fd.scss',
    '!**/_settings_ag.scss',
    '!**/agora.scss'
  ];

  return gulp.src(sass_folder).pipe(postcss(processors, {syntax: syntax_scss}));
});

// JavaScript
gulp.task('scripts', function() {
  return gulp.src('_js/app.js').pipe(jshint()).pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('compress', function() {
  return gulp.src('_js/app.js').pipe(babel({presets: ['es2015-script'], compact: true})).pipe(concat('theme.min.js')).pipe(uglify()).pipe(gulp.dest(BUILD_FOLDER + 'js'));
});

// Watch files for changes
gulp.task('watch', function() {
  gulp.watch('gulpfile.js', ['sass']);
  gulp.watch(SCRIPT_CSS_FOLDER + '_scss/**/*.scss', ['sass', 'sass-lint']);
  gulp.watch('_js/app.js', ['scripts', 'compress']);
});

// Default task
gulp.task('default', ['sass', 'scripts', 'compress', 'watch']);